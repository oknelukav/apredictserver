package ru.sberbank.apredict;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class APredictServer {

	public static void main(String[] args) {
		SpringApplication.run(APredictServer.class, args);
	}
}
