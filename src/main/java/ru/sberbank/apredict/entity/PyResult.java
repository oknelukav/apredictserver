package ru.sberbank.apredict.entity;

import java.io.Serializable;
import java.util.Objects;

public class PyResult implements Serializable {


	public PyResult(Boolean isanomaly_IF, Boolean isanomaly_AE, Double if_branches, Double recon_error, String uniqueID,
			String comment) {
		super();
		this.isanomaly_IF = isanomaly_IF;
		this.isanomaly_AE = isanomaly_AE;
		this.if_branches = if_branches;
		this.recon_error = recon_error;
		this.uniqueID = uniqueID;
		this.comment = comment;
	}
	private static final long serialVersionUID = -2872781080554901412L;

	private Boolean isanomaly_IF;
	private Boolean isanomaly_AE;
	private Double if_branches;
	private Double recon_error;
	private String uniqueID;
	private String comment;
	public Boolean getIsanomaly_IF() {
		return isanomaly_IF;
	}
	public void setIsanomaly_IF(Boolean isanomaly_IF) {
		this.isanomaly_IF = isanomaly_IF;
	}
	public Boolean getIsanomaly_AE() {
		return isanomaly_AE;
	}
	public void setIsanomaly_AE(Boolean isanomaly_AE) {
		this.isanomaly_AE = isanomaly_AE;
	}
	public Double getIf_branches() {
		return if_branches;
	}
	public void setIf_branches(Double if_branches) {
		this.if_branches = if_branches;
	}
	public Double getRecon_error() {
		return recon_error;
	}
	public void setRecon_error(Double recon_error) {
		this.recon_error = recon_error;
	}
	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "PyResult [isanomaly_IF=" + isanomaly_IF + ", isanomaly_AE=" + isanomaly_AE + ", if_branches="
				+ if_branches + ", recon_error=" + recon_error + ", uniqueID=" + uniqueID + ", comment=" + comment
				+ "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(uniqueID);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof PyResult)) {
			return false;
		}
		PyResult other = (PyResult) obj;
		return Objects.equals(uniqueID, other.uniqueID);
	}

	

}
