package ru.sberbank.apredict.entity;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

public class PyDataObject implements Serializable {
	public PyDataObject(Map<String, Object> fields) {
		super();
		this.fields = fields;
	}

	static final long serialVersionUID = -1857228393836384318L;

	private Map<String, Object> fields;

	public Map<String, Object> getFields() {
		return fields;
	}

	public String getUniqueID() {
		Object uid = fields.get("uniqueID");
		return uid!=null?uid.toString():null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getUniqueID());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof PyDataObject)) {
			return false;
		}
		PyDataObject other = (PyDataObject) obj;
		return Objects.equals(getUniqueID(), other.getUniqueID());
	}

	@Override
	public String toString() {
		return "PyDataObject [fields=" + fields.toString() + "]";
	}

}
