package ru.sberbank.apredict.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
public class PyDataObjectStorageService {

	private final Logger log = LoggerFactory.getLogger(PyDataObjectStorageService.class);
	private final Queue<PyDataObject> pyDataObjects = new ConcurrentLinkedQueue<PyDataObject>();

	public void store(PyDataObject pyDataObject) {
		pyDataObjects.add(pyDataObject);
	}

	// return null if queue is empty
	public PyDataObject take() {
		return pyDataObjects.poll();
	}

}
