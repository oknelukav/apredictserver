package ru.sberbank.apredict;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "kafka.server")
public class KafkaConfig {
	private String bootstrap;
	private String pyDataObjectTopic;
	private String pyResultTopic;
	private String pyDataObjectGroupId;

	public String getBootstrap() {
		return bootstrap;
	}

	public void setBootstrap(String bootstrap) {
		this.bootstrap = bootstrap;
	}

	public String getPyDataObjectTopic() {
		return pyDataObjectTopic;
	}

	public void setPyDataObjectTopic(String pyDataObjectTopic) {
		this.pyDataObjectTopic = pyDataObjectTopic;
	}

	public String getPyResultTopic() {
		return pyResultTopic;
	}

	public void setPyResultTopic(String pyResultTopic) {
		this.pyResultTopic = pyResultTopic;
	}

	public String getPyDataObjectGroupId() {
		return pyDataObjectGroupId;
	}

	public void setPyDataObjectGroupId(String pyDataObjectGroupId) {
		this.pyDataObjectGroupId = pyDataObjectGroupId;
	}

}
