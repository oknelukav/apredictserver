package ru.sberbank.apredict.outgoing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.sberbank.apredict.KafkaConfig;
import ru.sberbank.apredict.entity.PyResult;

@Service
public class PyResultKafkaProducer {
	@Autowired
	KafkaConfig kafkaConfig;

	@Autowired
	private KafkaTemplate<String, PyResult> pyResultKafkaTemplate;

	private final Logger log = LoggerFactory.getLogger(PyResultKafkaProducer.class);

	public void send(PyResult pyResult) {
		pyResultKafkaTemplate.send(kafkaConfig.getPyResultTopic(), pyResult);
		log.debug("### sent PyResult to Kafka: {} ", pyResult.toString());
	}
}
