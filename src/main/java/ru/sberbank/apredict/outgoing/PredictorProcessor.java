package ru.sberbank.apredict.outgoing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import hex.genmodel.MojoModel;
import hex.genmodel.easy.EasyPredictModelWrapper;
import hex.genmodel.easy.RowData;
import hex.genmodel.easy.exception.PredictException;
import hex.genmodel.easy.exception.PredictUnknownCategoricalLevelException;
import hex.genmodel.easy.prediction.AnomalyDetectionPrediction;
import hex.genmodel.easy.prediction.AutoEncoderModelPrediction;
import ru.sberbank.apredict.entity.PyDataObject;
import ru.sberbank.apredict.entity.PyDataObjectStorageService;
import ru.sberbank.apredict.entity.PyResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.apache.camel.Exchange;
import org.apache.camel.PropertyInject;

@Component
public class PredictorProcessor {

	@PropertyInject("apredictserver.mojo.dir")
	private String apredictserverMojoDir;

	@PropertyInject("apredictserver.mojo.filename.dl")
	private String apredictserverMojoFilenameDL;

	@PropertyInject("apredictserver.mojo.filename.if")
	private String apredictserverMojoFilenameIF;

	@PropertyInject("apredictserver.dl.threshold") // anomaly if mse > threshold
	private String dLthreshold;

	@PropertyInject("apredictserver.if.threshold") // anomaly if score < threshold
	private String iFthreshold;

	final SimpleDateFormat formatterDateFromDD = new SimpleDateFormat("dd/MM/yyyy");

	private EasyPredictModelWrapper modelDL;
	private EasyPredictModelWrapper modelIF;

	@PropertyInject("apredictserver.sendToKAFKA")
	private boolean apredictserverSendToKAFKA;

	final Logger logger = LoggerFactory.getLogger(PredictorProcessor.class);

	@Autowired
	PyDataObjectStorageService pyDataObjectStorageService;

	@Autowired
	PyResultKafkaProducer pyResultKafkaProducer;

	private Map<String, String> mapToMOJO = new HashMap<String, String>();

	private static int count = 0;
	@PostConstruct
	private void postConstruct() throws IOException {
		String dEAPLEARNINGMOJOMODELPATH = apredictserverMojoDir + System.getProperty("file.separator")
				+ apredictserverMojoFilenameDL;
		String iFORESTMOJOMODELPATH = apredictserverMojoDir + System.getProperty("file.separator")
				+ apredictserverMojoFilenameIF;
		EasyPredictModelWrapper.Config configDL = new EasyPredictModelWrapper.Config()
				.setModel(MojoModel.load(dEAPLEARNINGMOJOMODELPATH));
		modelDL = new EasyPredictModelWrapper(configDL);

		EasyPredictModelWrapper.Config configIF = new EasyPredictModelWrapper.Config()
				.setModel(MojoModel.load(iFORESTMOJOMODELPATH));
		modelIF = new EasyPredictModelWrapper(configIF);

		logger.info("### modelDL.getModelCategory(): {}, modelIF.getModelCategory(): {} are uploaded",
				modelDL.getModelCategory(), modelIF.getModelCategory());

		mapToMOJO.put("tradefinoperation", "Trade_fin_operation");
		mapToMOJO.put("tradernum", "Trader_num");
		mapToMOJO.put("tradecptynum", "Trade_counterparty_num");
		mapToMOJO.put("trademarket", "Trade_market");
		mapToMOJO.put("tradedirection", "Trade_direction");
		mapToMOJO.put("tradeccy1", "Trade_ccy_1");
		mapToMOJO.put("tradeccy2", "Trade_ccy_2");
		mapToMOJO.put("paymentamount", "Payment_amount");
		mapToMOJO.put("paymentccy", "Payment_ccy");
		mapToMOJO.put("paymentdirection", "Payment_direction");
		mapToMOJO.put("paymentnetting", "Payment_netting");
		mapToMOJO.put("paymentfinoperation", "Payment_fin_op");
	}

	private void addDays(Calendar cal, Date paymentDate, RowData row) {
		cal.setTime(paymentDate);
		Integer dW = cal.get(Calendar.DAY_OF_WEEK);
		Integer dM = cal.get(Calendar.DAY_OF_MONTH);
		dW = dW - 1;
		if (dW == 0) {
			dW = 7;
		}
		row.put("day_of_week", dW.toString());
		row.put("day_of_month", dM.toString());
	}

	public void process(Exchange xchg) throws PredictException {
		do {
			PyDataObject pyDataObject = pyDataObjectStorageService.take();
			if (pyDataObject != null) {
				if (count % 1000 == 0) {
					logger.info("###### count={}", count);
				}
				count++;
				logger.debug("### pyDataObject: {}", pyDataObject.toString());
				RowData row = new RowData();
				Calendar cal = Calendar.getInstance();

				for (String fieldName : pyDataObject.getFields().keySet()) {
					Object fieldValue = pyDataObject.getFields().get(fieldName);
					switch (fieldName) {
					case "uniqueID":
					case "Payment_date": // from CSV - we need to omit this field
						break;
					case "paymentdate": // from Kafka - we need to calculate day_of_week and day_of_month and put them to RowData
						Date paymentDate = new Date((Long) fieldValue);
						addDays(cal, paymentDate, row);
						break;
					default:
						String fieldNameMapped = mapToMOJO.get(fieldName);
						row.put((fieldNameMapped != null) ? fieldNameMapped : fieldName, fieldValue.toString());
						break;
					}
				}

				logger.debug("### RowData is sent to model: {}", row.toString());

				AutoEncoderModelPrediction pDL = null;
				AnomalyDetectionPrediction pIF = null;
				String predictExceptionMessage = "";
				Double dlMse = 0.0;
				Double ifScore = 0.0;
				Boolean isIFAnomaly = Boolean.FALSE;
				Boolean isDLAnomaly = Boolean.FALSE;
				try {
					pDL = modelDL.predictAutoEncoder(row);
					pIF = modelIF.predictAnomalyDetection(row);
					dlMse = pDL.mse;
					ifScore = pIF.score;
					isIFAnomaly = (ifScore < Double.valueOf(iFthreshold)) ? Boolean.TRUE : Boolean.FALSE;
					isDLAnomaly = (dlMse > Double.valueOf(dLthreshold)) ? Boolean.TRUE : Boolean.FALSE;
				} catch (PredictUnknownCategoricalLevelException e) {
					predictExceptionMessage = e.getMessage();
				}

				logger.debug(
						"### recieved model prediction:  pDL.mse={}  isDLAnomaly={}  pIF.score={} isIFAnomaly={}   predictExceptionMessage={}",
						dlMse, isDLAnomaly, ifScore, isIFAnomaly, predictExceptionMessage);
				if (isDLAnomaly && isIFAnomaly) {
					logger.info("######## Anomaly boolean is detected with parameters dlMse={}  ifScore={} for row={}", dlMse, ifScore, row.toString());
				}
				if (!predictExceptionMessage.isEmpty()) {
					logger.info("######## Anomaly categorical is detected with parameters dlMse={}  ifScore={} predictExceptionMessage={} for row={}", dlMse, ifScore, predictExceptionMessage, row.toString());
				}
				if (apredictserverSendToKAFKA) {
					PyResult pyResult = new PyResult(isIFAnomaly, isDLAnomaly, ifScore, dlMse, pyDataObject.getUniqueID(),
							predictExceptionMessage);
					pyResultKafkaProducer.send(pyResult);
				}
			} else
				break;
		} while (true);
	}
}
