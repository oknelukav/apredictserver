package ru.sberbank.apredict.outgoing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class PredictorRoute extends RouteBuilder {
	
	final Logger logger = LoggerFactory.getLogger(PredictorRoute.class);

	@Autowired
	PredictorProcessor predictorProcessor;

	@Override
	public void configure() {
		try {
			from("quartz://timerPredictor?trigger.repeatInterval={{apredictserver.timers.repeatInterval}}&trigger.repeatCount=-1&stateful=true")
					.bean(predictorProcessor, "process").end();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
