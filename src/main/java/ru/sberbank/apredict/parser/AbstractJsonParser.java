package ru.sberbank.apredict.parser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractJsonParser<T> implements JsonParser<T> {
    private final static ObjectMapper JSON_MAPPER = new ObjectMapper();
    private final Logger log = LoggerFactory.getLogger(AbstractJsonParser.class);

    @Override
    public Optional<T> parse(String raw) {
        return convert(() -> JSON_MAPPER.readValue(raw, new TypeReference<Map<String, Object>>() {
        }));
    }

    @Override
    public Optional<T> parse(InputStream raw) {
        return convert(() -> JSON_MAPPER.readValue(raw, new TypeReference<Map<String, Object>>() {
        }));
    }

    private Optional<T> convert(Reader reader) {
        try {
            return Optional.of(convert(reader.read()));
        } catch (IOException e) {
            log.error("{} parse exception", getParsedClass().getName(), e);
            return Optional.empty();
        }
    }

    abstract protected T convert(Map<String, Object> fields);

    @FunctionalInterface
    private interface Reader {
        Map<String, Object> read() throws IOException;
    }
}
