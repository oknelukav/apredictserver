package ru.sberbank.apredict.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JsonParserService {
	
	private final Logger log = LoggerFactory.getLogger(JsonParserService.class);
	
    @SuppressWarnings("rawtypes")
    private Map<Class<?>, JsonParser> parsers;

    
    //Spring will try to find all @Component inherited from JsonParser and fill List with these objects. In this context we have two such components: 
    //MatchingObjectParser and MatchingTriggerParser
    @SuppressWarnings("rawtypes")
    @Autowired
    private void setJsonParsers(List<JsonParser> jsonParsers) {
        parsers = jsonParsers.stream().collect(Collectors.toMap(JsonParser::getParsedClass, x -> x));
    }

    @SuppressWarnings("unchecked")
    public <T> Optional<T> parse(String raw, Class<T> clazz) {
        return parsers.get(clazz).parse(raw);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> Optional<T> parse(InputStream raw, Class<T> clazz) {
        JsonParser parser = parsers.get(clazz);
        if (parser == null) {
            throw new RuntimeException("There is no parser for " + clazz.getName());
        }
        return parser.parse(raw);
    }
}
