package ru.sberbank.apredict.parser;

import org.springframework.stereotype.Component;
import ru.sberbank.apredict.entity.PyDataObject;
import java.util.Map;

@Component
public class PyDataObjectParser extends AbstractJsonParser<PyDataObject> {

	@Override
	public Class<PyDataObject> getParsedClass() {
		return PyDataObject.class;
	}

	@Override
	protected PyDataObject convert(Map<String, Object> fields) {
		return new PyDataObject(fields);
	}
}
