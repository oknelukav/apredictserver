package ru.sberbank.apredict.parser;

import java.io.InputStream;
import java.util.Optional;

public interface JsonParser<T> {
    Class<T> getParsedClass();

    Optional<T> parse(String raw);

    Optional<T> parse(InputStream raw);
}
