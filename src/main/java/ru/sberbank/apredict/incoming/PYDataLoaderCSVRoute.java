package ru.sberbank.apredict.incoming;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.CsvDataFormat;

@Component
@ConditionalOnProperty(name = "kafka.enable", havingValue = "false")
public class PYDataLoaderCSVRoute extends RouteBuilder {

	@Autowired
	PYDataLoaderProcessor pyDataLoaderProcessor;

	@Override
	public void configure() {
		CsvDataFormat csv = new CsvDataFormat();
		csv.setDelimiter(";");
		csv.setLazyLoad(Boolean.toString(Boolean.TRUE));
		csv.setUseMaps(Boolean.toString(Boolean.TRUE));

		from("file://{{apredictserver.incoming.csv.dir}}"
				+ "?delay={{apredictserver.incomingcsv.repeatInterval}}&delete=true").unmarshal(csv).split(body())
						.streaming().bean(pyDataLoaderProcessor, "processCSV").end();
	}
}
