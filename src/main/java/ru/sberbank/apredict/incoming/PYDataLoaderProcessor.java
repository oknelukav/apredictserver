package ru.sberbank.apredict.incoming;

import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.apache.camel.Exchange;
import org.apache.camel.PropertyInject;

import ru.sberbank.apredict.entity.PyDataObject;
import ru.sberbank.apredict.entity.PyDataObjectStorageService;
import ru.sberbank.apredict.parser.JsonParserService;

@Component
public class PYDataLoaderProcessor {

	@Autowired
	JsonParserService jsonParserService;

	@Autowired
	PyDataObjectStorageService pyDataObjectStorageService;
	
	@PropertyInject("apredictserver.csvloading.delay")
	private Integer apredictserverCsvLoadingDelay;

	final Logger logger = LoggerFactory.getLogger(PYDataLoaderProcessor.class);

	PYDataLoaderProcessor() throws IOException {

	}

	@SuppressWarnings("unused")
	public void consume(String pyDataRaw) {
		logger.debug("### received pyDataRaw from aserver: {}", pyDataRaw);
		PyDataObject pyDataObj = jsonParserService.parse(pyDataRaw, PyDataObject.class).get();
		logger.debug("### converted to pyDataObj: {}", pyDataObj.toString());
		pyDataObjectStorageService.store(pyDataObj);
	}

	@SuppressWarnings("unchecked")
	public void processCSV(Exchange xchg) throws InterruptedException {

		Map<String, Object> csvRecord = (HashMap<String, Object>) xchg.getIn().getBody();
		logger.debug("###### csvRecord (PYData): {}", csvRecord);
		PyDataObject pyDataObj = new PyDataObject(csvRecord);
		logger.debug("### converted to pyDataObj: {}", pyDataObj.toString());
		Thread.sleep(apredictserverCsvLoadingDelay);
		pyDataObjectStorageService.store(pyDataObj);
	}
}
