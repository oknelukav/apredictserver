package ru.sberbank.apredict.incoming;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import ru.sberbank.apredict.KafkaConfig;
import org.apache.camel.builder.RouteBuilder;

@Component
@ConditionalOnProperty(name = "kafka.enable", havingValue = "true")
public class PYDataLoaderKAFKARoute extends RouteBuilder {

	@Autowired
	KafkaConfig kafkaConfig;

	@Autowired
	PYDataLoaderProcessor pyDataLoaderProcessor;

	@Override
	public void configure() {
		try {
			from("kafka:" + kafkaConfig.getPyDataObjectTopic() + "?brokers=" + kafkaConfig.getBootstrap() + "&groupId="
					+ kafkaConfig.getPyDataObjectGroupId() + "&autoOffsetReset=latest&consumersCount=1")
							.bean(pyDataLoaderProcessor, "consume").end();
		} catch (Exception e) {
			log.error("Kafka getting matching object exception", e);
		}
	}
}
