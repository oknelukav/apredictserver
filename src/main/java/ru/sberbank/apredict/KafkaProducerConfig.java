package ru.sberbank.apredict;

import com.google.common.collect.ImmutableMap;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import ru.sberbank.apredict.entity.PyResult;

@Configuration
public class KafkaProducerConfig {
	@Autowired
	KafkaConfig properties;

	@Bean
	public ProducerFactory<String, PyResult> matchingResultProducerFactory() {
		return new DefaultKafkaProducerFactory<>(ImmutableMap.of(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
				properties.getBootstrap(), ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class,
				ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class));
	}

	@Bean
	public KafkaTemplate<String, PyResult> matchingResultKafkaTemplate(
			@Autowired ProducerFactory<String, PyResult> matchingResultProducerFactory) {
		return new KafkaTemplate<>(matchingResultProducerFactory);
	}
}
